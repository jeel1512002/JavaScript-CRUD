import createViewCard from "./modules/viewCard.js";
const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get("id");
const product = JSON.parse(localStorage.getItem("products")).find((i) => i.id == id);

document.title = `Product Id: ${id}`

document.querySelector(".container").innerHTML = product
    ? createViewCard(product)
    : "<h1>No Data</h1>";
