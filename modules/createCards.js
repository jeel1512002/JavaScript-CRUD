const createCards = (products) => {
    let cards = "";
    products.forEach((product) => {
        cards += `
            <div class="product-card">
                <div class="product-img">
                    <img src="${product.image.path}" alt="${product.name}" />
                </div>
                <hr />
                <h2 class="id">#${product.id}</h2>
                <h2 class="name">${product.name}</h2>
                <h2 class="price">$${product.price}</h2>
                <div class="buttons">
                    <button class="edit btn" data-id="${product.id}"></button>
                    <button class="view btn" data-id="${product.id}"></button>
                    <button class="delete btn" data-id="${product.id}"></button>
                </div>
            </div>`;
    });
    return cards;
}



export default createCards;