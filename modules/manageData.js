export const setData = (products) => {
    localStorage.setItem("products", JSON.stringify(products));
}

export const getData = () => {
    let products = []
    if (!localStorage.getItem("products")) {
        products = [
            {
                id: 1,
                name: "apple",
                price: 2,
                description: "fresh apples",
                image: {
                    path: "./images/apple.png",
                    name: "apple.png",
                },
            },
            {
                id: 2,
                name: "banana",
                price: 1,
                description: "fresh banana",
                image: {
                    path: "./images/banana.png",
                    name: "banana.png",
                },
            },
            {
                id: 3,
                name: "orange",
                price: 3,
                description: "fresh orange",
                image: {
                    path: "./images/orange.png",
                    name: "orange.png",
                },
            },
            {
                id: 4,
                name: "apple",
                price: 2.5,
                description: "fresh apples",
                image: {
                    path: "./images/apple.png",
                    name: "apple.png",
                },
            },
            {
                id: 5,
                name: "apple",
                price: 3,
                description: "fresh apples",
                image: {
                    path: "./images/apple.png",
                    name: "apple.png",
                },
            },
        ];
        setData(products)
    } else {
        products = JSON.parse(localStorage.getItem("products"));
    }
    return products;
}
