const doDelete = (id, products) => {
    products = products.filter((product) => {
        return product.id != id;
    });
    return products;
}
export default doDelete;