import {
    productId,
    productName,
    productPrice,
    productDescription,
    uploadBtn,
    submit
} from "../constant.js";

const doEdit = (id, products) => {
    let productIndex = products.findIndex((i) => i.id == id);
    submit.value = "Edit";
    productId.value = products[productIndex].id;
    productName.value = products[productIndex].name;
    productPrice.value = products[productIndex].price;
    productDescription.value = products[productIndex].description;
    uploadBtn.innerHTML = `Uploaded Img: ${products[productIndex].image.name}`;
    return productIndex
}

export default doEdit;