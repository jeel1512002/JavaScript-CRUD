const doView = (id) => {
    console.log("view clicked", id);
    location.href = `/view.html?id=${id}`
}

export default doView;