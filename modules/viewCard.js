const createViewCard = (product) => {
    return (`
        <div class="detail">
            <div>
                <span class="title">Product Name: </span>
                <span>${product.name}</span>
            </div>
            <div>
                <span class="title">ID: </span>
                <span>#${product.id}</span>
            </div>
            <div>
                <span class="title">Price: </span>
                <span>$${product.price}</span>
            </div>
            <div>
                <span class="title">Description:</span>
                <div id="description">${product.description}</div>
            </div>  
        </div>
        <div class="image">
            <img src="${product.image.path}" alt="${product.image.name}">
        </div>`
    );
};

export default createViewCard;