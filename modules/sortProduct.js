const sortProduct = (type, products) => {
    let sortProductArr = products;
    sortProductArr.sort((a, b) => {
        return a[type] < b[type] ? -1 : 1;
    });
    return sortProductArr;
}

export default sortProduct;