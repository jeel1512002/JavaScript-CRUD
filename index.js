import {
    productId,
    productName,
    productPrice,
    productDescription,
    uploadBtn,
    productImage,
    submit,
    sortBy,
    search,
    productSlider,
    prev,
    next,
    stringRegEx
} from "./constant.js";

import createCards from "./modules/createCards.js";
import doEdit from "./modules/doEdit.js";
import doDelete from "./modules/doDelete.js";
import doView from "./modules/doView.js";
import { getData, setData } from "./modules/manageData.js";
import filterById from "./modules/filterById.js";
import sortProduct from "./modules/sortProduct.js";

let imageData, productIndex, position;
let cardArr = [];
let filterArr = [];

let products = getData();
show(products);

document.querySelector("form").onsubmit = doSubmit;

search.addEventListener("input", () => {
    filterArr = filterById(products)
    show(filterArr);
});

sortBy.addEventListener("click", (e) => {
    switch (e.target.id) {
        case "sort-id":
            show(sortProduct("id", search.value ? filterArr : products));
            break;
        case "sort-name":
            show(sortProduct("name", search.value ? filterArr : products));
            break;
        case "sort-price":
            show(sortProduct("price", search.value ? filterArr : products));
            break;
        default:
            break;
    }
});

productSlider.addEventListener("click", (e) => {
    if (e.target.classList.contains("edit")) {
        productIndex = doEdit(e.target.dataset.id, products);
    }
    if (e.target.classList.contains("delete")) {
        products = doDelete(e.target.dataset.id, products);
        setData(products);
        show(products);
    }
    if (e.target.classList.contains("view")) {
        doView(e.target.dataset.id);
    }
});

// ====================================== Product carousel ======================================

productSlider.addEventListener("mouseover", (e) => {
    stopSlider();
});

productSlider.addEventListener("mouseout", (e) => {
    startSlider();
});

prev.onclick = prevFun;
next.onclick = nextFun;

let flag = false;
let time;

startSlider();

function startSlider() {
    time = setInterval(() => {
        if (position && flag) {
            prevFun();
        } else {
            flag = false;
            if (position >= (cardArr.length - 5) * -18 && !flag) {
                nextFun();
            } else {
                flag = true;
            }
        }
    }, 1800);
}

function stopSlider() {
    clearInterval(time);
}

function prevFun() {
    position += 18;
    if (!position) {
        prev.classList.toggle("remove", true);
    } else {
        prev.classList.toggle("remove", false);
    }

    if (position > (cardArr.length - 5) * 18) {
        next.classList.toggle("remove", true);
    } else {
        next.classList.toggle("remove", false);
    }
    cardArr.forEach((e) => {
        e.style.transform = `translateX(${position}rem)`;
    });
}

function nextFun() {
    position -= 18;
    if (!position) {
        prev.classList.toggle("remove", true);
    } else {
        prev.classList.toggle("remove", false);
    }

    if (position < (cardArr.length - 5) * -18) {
        next.classList.toggle("remove", true);
    } else {
        next.classList.toggle("remove", false);
    }
    cardArr.forEach((e) => {
        e.style.transform = `translateX(${position}rem)`;
    });
}

// ====================================== Show Products ======================================

function show(products) {
    if (!products.length) {
        position = 0;
        cardArr = document.querySelectorAll(".product-card");
        !position || cardArr.length < 5
            ? prev.classList.toggle("remove", true)
            : prev.classList.toggle("remove", false);
        position < (cardArr.length - 5) * -18
            ? next.classList.toggle("remove", true)
            : next.classList.toggle("remove", false);
        productSlider.innerHTML = "<h1>No Data</h1>";
    } else {
        position = 0;
        productSlider.innerHTML = createCards(products);
        cardArr = document.querySelectorAll(".product-card");
        !position || cardArr.length < 5
            ? prev.classList.toggle("remove", true)
            : prev.classList.toggle("remove", false);
        position < (cardArr.length - 5) * -18
            ? next.classList.toggle("remove", true)
            : next.classList.toggle("remove", false);
    }
}

// ====================================== Add & Edit Product ======================================

productImage.onchange = () => {
    if (productImage.files[0].size > 1048576) {
        alert("Size of file should be <= 1MB");
        productImage.value = "";
        return;
    }
    let file = new FileReader();
    uploadBtn.innerHTML = `Uploaded Img: ${productImage.files[0].name}`;
    file.readAsDataURL(productImage.files[0]);
    file.onload = function () {
        imageData = file.result;
    };
};

function isStringOnly(data) {
    return stringRegEx.test(data);
}

function doSubmit(e) {
    e.preventDefault();

    if (submit.value == "Submit") {
        let product = {};
        product.id = parseInt(productId.value);

        if (products.find((i) => i.id == product.id)) {
            alert(`ID: ${product.id} already exist`);
            return;
        }

        if (!isStringOnly(productName.value)) {
            alert("Enter valid name");
            return;
        }
        product.name = productName.value;
        product.price = parseFloat(productPrice.value);
        product.description = productDescription.value;
        if (!productImage.files[0]) {
            alert("please upload image");
            return;
        }
        product.image = {};
        product.image.name = uploadBtn.innerHTML.slice(13);
        product.image.path = imageData;
        products.push(product);
    } else {
        products[productIndex].id = parseInt(productId.value);
        if (
            products.find(
                (i, index) => i.id == products[productIndex].id && index != productIndex
            )
        ) {
            alert(`ID: ${productId.value} already exist`);
            return;
        }

        if (!isStringOnly(productName.value)) {
            alert("Enter valid name");
            return;
        }

        products[productIndex].name = productName.value;
        products[productIndex].price = parseFloat(productPrice.value);
        products[productIndex].description = productDescription.value;
        products[productIndex].image.path =
            imageData ?? products[productIndex].image.path;
        products[productIndex].image.name = uploadBtn.innerHTML.slice(13);
        submit.value = "Submit";
    }

    productId.value = "";
    productName.value = "";
    productPrice.value = "";
    productDescription.value = "";
    productName.value = "";
    productImage.value = "";
    uploadBtn.innerHTML = "Upload Image";
    setData(products);
    show(products);
}